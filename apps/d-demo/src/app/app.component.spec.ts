import { TestBed, async } from '@angular/core/testing'
import { EffectsModule } from '@ngrx/effects'
import { StoreModule } from '@ngrx/store'
import { NxModule } from '@nrwl/nx'
import { AppComponent } from './app.component'
import { RouterTestingModule } from '@angular/router/testing'
import { FeatureMapModule } from '@ui/feature/map'
import { FeaturePanelOneModule } from '@ui/feature/panel-one'
import { UiCharteModule } from '@ui/ui/charte'

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FeatureMapModule,
        FeaturePanelOneModule,
        UiCharteModule,
        NxModule.forRoot(),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
      ],
      declarations: [AppComponent],
      providers: [],
    }).compileComponents()
  }))

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app).toBeTruthy()
  })
})
