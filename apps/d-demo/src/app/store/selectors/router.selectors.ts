import * as fromNgrxRouter from '@ngrx/router-store'
import { createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store'

import { State } from '../../../app/store'
import { environment } from '../../../environments/environment'
import * as fromRouter from '../reducers/router.reducer'

export const getRouterState = createFeatureSelector<
  fromNgrxRouter.RouterReducerState<fromRouter.RouterStateUrl>
>('router')

export const getQueryParams = createSelector(
  getRouterState,
  router => {
    return router && router.state.queryParams
  }
)

export const getFullUrl = createSelector(
  getRouterState,
  router => {
    return router && location.origin + router.state.url
  }
)
