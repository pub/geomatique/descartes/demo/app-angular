import * as fromNgrxRouter from '@ngrx/router-store'
import { ActionReducerMap } from '@ngrx/store'

import * as fromRouter from './router.reducer'

export interface State {
  router: fromNgrxRouter.RouterReducerState<fromRouter.RouterStateUrl>
}

export const reducers: ActionReducerMap<State> = {
  router: fromNgrxRouter.routerReducer,
}

export * from './router.reducer'
