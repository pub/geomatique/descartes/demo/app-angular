import * as fromActions from './router.action'

describe('Router Actions', () => {
  describe('Go Action', () => {
    it('should create an action', () => {
      const payload: any = {
        path: ['.'],
        query: {
          id: 10,
        },
        extras: {
          queryParamsHandling: 'merge',
        },
      }
      const action = new fromActions.Go(payload)

      expect({ ...action }).toEqual({
        payload,
        type: fromActions.GO,
      })
    })
  })

  describe('Back Action', () => {
    it('should create an action', () => {
      const action = new fromActions.Back()

      expect({ ...action }).toEqual({
        type: fromActions.BACK,
      })
    })
  })

  describe('forward Action', () => {
    it('should create an action', () => {
      const action = new fromActions.Forward()

      expect({ ...action }).toEqual({
        type: fromActions.FORWARD,
      })
    })
  })
})
