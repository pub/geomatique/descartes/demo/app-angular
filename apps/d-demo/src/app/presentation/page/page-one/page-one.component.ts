import { Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { Store } from '@ngrx/store'
import * as fromStore from '../../../store'

@Component({
  selector: 'ui-page-one',
  templateUrl: './page-one.component.html',
  styleUrls: ['./page-one.component.scss'],
})
export class PageOneComponent implements OnInit {
  height: number = 400
  @ViewChild('container') private container_: ElementRef

  constructor(private _store: Store<fromStore.State>) {}

  ngOnInit() {
    this.recomputeHeight()
  }

  recomputeHeight() {
    const topOffset = this.container_.nativeElement.getBoundingClientRect().top
    this.height = window.innerHeight - topOffset - window.scrollY - 40
  }

  // todo: remove
  back(): void {
    this._store.dispatch(new fromStore.Back())
  }
}
