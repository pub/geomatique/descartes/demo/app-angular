import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { PanelComponent } from './panel.component'

describe('PanelComponent', () => {
  let component: PanelComponent
  let fixture: ComponentFixture<PanelComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PanelComponent],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelComponent)
    component = fixture.componentInstance
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should not have a title block when none provided', () => {
    fixture.detectChanges()
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelectorAll('.panel-title').length).toEqual(0)
  })

  it('should have the correct title', () => {
    component.panelTitle = 'Test title 1 <span>aa</span>'
    fixture.detectChanges()
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelector('.panel-title').textContent.trim()).toEqual(
      component.panelTitle
    )
  })

  it('should have a close button if collapsible', () => {
    component.collapsible = true
    fixture.detectChanges()
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelectorAll('button.close').length).toEqual(1)
  })

  it('should be initially collapsed if required', () => {
    component.initiallyCollapsed = true
    fixture.detectChanges()
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelector('.panel-container').className).toContain(
      'collapsed'
    )
  })

  it('should not be initially collapsed if required', () => {
    component.initiallyCollapsed = false
    fixture.detectChanges()
    const compiled = fixture.debugElement.nativeElement
    expect(
      compiled.querySelector('.panel-container').className.indexOf('collapsed')
    ).toEqual(-1)
  })
})
