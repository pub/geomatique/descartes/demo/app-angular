import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core'

@Component({
  selector: 'ui-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PanelComponent implements OnInit {
  @Input() panelTitle: string
  @Input() collapsible: boolean = false
  @Input() initiallyCollapsed: boolean = false
  collapsedState: boolean

  constructor() {}

  ngOnInit() {
    this.collapsedState = this.initiallyCollapsed
  }

  toggleCollapsedState() {
    this.collapsedState = !this.collapsedState
  }
}
