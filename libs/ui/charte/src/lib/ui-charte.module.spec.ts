import { async, TestBed } from '@angular/core/testing'
import { UiCharteModule } from './ui-charte.module'

describe('UiCharteModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UiCharteModule],
    }).compileComponents()
  }))

  it('should create', () => {
    expect(UiCharteModule).toBeDefined()
  })
})
