import { async, TestBed } from '@angular/core/testing'
import { FeaturePanelOneModule } from './feature-panel-one.module'

describe('FeaturePanelOneModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FeaturePanelOneModule],
    }).compileComponents()
  }))

  it('should create', () => {
    expect(FeaturePanelOneModule).toBeDefined()
  })
})
