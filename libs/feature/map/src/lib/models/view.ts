export interface View {
  center: number[]
  zoom: number
}
