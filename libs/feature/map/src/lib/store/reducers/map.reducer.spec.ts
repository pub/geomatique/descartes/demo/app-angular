import * as fromActions from '../actions/map.actions'
import * as fromMap from './map.reducer'
import { View } from '../../models/view'

describe('Map Reducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const { initialState } = fromMap
      const action = {} as any
      const state = fromMap.reducer(undefined, action)

      expect(state).toBe(initialState)
    })
  })

  describe('SET_VIEW action', () => {
    it('should set center and zoom', () => {
      const { initialState } = fromMap
      const payload: View = {
        center: [0, 0],
        zoom: 2,
      }
      const action = new fromActions.SetView(payload)
      const state = fromMap.reducer(initialState, action)

      expect(state.center).toEqual(payload.center)
      expect(state.zoom).toEqual(payload.zoom)
    })
  })

  describe('SET_Zoom action', () => {
    it('should set zoom', () => {
      const { initialState } = fromMap
      const payload = 2
      const action = new fromActions.SetZoom(payload)
      const state = fromMap.reducer(initialState, action)

      expect(state.zoom).toEqual(payload)
    })
  })
})
