import { Action } from '@ngrx/store'
import { View } from '../../models/view'

export const SET_VIEW = '[Map] Set view'
export const SET_ZOOM = '[Map] Set zoom'

export class SetView implements Action {
  readonly type = SET_VIEW

  constructor(public payload: View) {}
}

export class SetZoom implements Action {
  readonly type = SET_ZOOM

  constructor(public payload: number) {}
}

export type MapActions = SetView | SetZoom
