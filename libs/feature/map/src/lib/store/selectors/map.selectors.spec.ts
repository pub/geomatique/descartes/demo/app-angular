import { combineReducers, select, Store, StoreModule } from '@ngrx/store'
import { TestBed } from '@angular/core/testing'

import { MAP_FEATURE_KEY } from '../../store'
import * as fromActions from '../actions/'
import * as fromReducers from '../reducers/index'
import * as fromSelectors from '../selectors/map.selectors'
import * as fromMap from '../reducers/map.reducer'
import { View } from '../../models/view'

describe('Map Selectors', () => {
  let store: Store<fromReducers.MapState>

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          [MAP_FEATURE_KEY]: fromReducers.reducer,
        }),
      ],
    })

    store = TestBed.get(Store)
  })

  describe('getMapState', () => {
    it('should return state of map store slice', () => {
      let result

      store
        .pipe(select(fromSelectors.getMapState))
        .subscribe(value => (result = value))

      expect(result).toEqual(fromReducers.initialState)

      store.dispatch(new fromActions.SetZoom(3))

      expect(result).toEqual({
        center: fromReducers.initialState.center,
        zoom: 3,
      })
    })
  })

  describe('getMapCenter', () => {
    it('should return the map center state', () => {
      let result

      store
        .pipe(select(fromSelectors.getMapCenter))
        .subscribe(value => (result = value))

      expect(result).toEqual(fromReducers.initialState.center)

      store.dispatch(
        new fromActions.SetView({
          center: [0, 0],
          zoom: 3,
        })
      )

      expect(result).toEqual([0, 0])
    })
  })

  describe('getMapZoom', () => {
    it('should return the map zoom state', () => {
      let result
      const payload = 3

      store
        .pipe(select(fromSelectors.getMapZoom))
        .subscribe(value => (result = value))

      expect(result).toEqual(fromReducers.initialState.zoom)

      store.dispatch(new fromActions.SetZoom(payload))

      expect(result).toEqual(payload)
    })
  })

  describe('getMapView', () => {
    it('should return state of map store slice', () => {
      let result
      const payload: View = {
        center: [0, 0],
        zoom: 3,
      }

      store
        .pipe(select(fromSelectors.getMapView))
        .subscribe(value => (result = value))

      expect(result).toEqual({
        center: fromReducers.initialState.center,
        zoom: fromReducers.initialState.zoom,
      })

      store.dispatch(new fromActions.SetView(payload))

      expect(result).toEqual(payload)
    })
  })
})
